$.widget("custom.combobox", {
    _create: function() {
        this.wrapper = $("<div>").addClass("b-combobox").insertAfter(this.element);
        this.element.hide();

        this._createAutocomplete();
        this._createShowAllButton();
    },

    _createAutocomplete: function() {
        var selected = this.element.children(":selected"),
            value = selected.val() ? selected.text() : "";
        this.input = $("<input>")
            .appendTo(this.wrapper)
            .val(value)
            .attr("placeholder", this.element.find("option:disabled:selected:first").html() || "")
            .addClass("form-control ui-widget ui-widget-content ui-state-default ui-corner-left")
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: $.proxy(this, "_source"),
                position: {
                    at: 'left bottom+2px',
                }
            }).on('focusout', (event)=> {
              event.preventDefault();
              this.element.valid();
            });

        this._on(this.input, {
            autocompleteselect: function(event, ui) {
                ui.item.option.selected = true;
                this._trigger("select", event, {
                    item: ui.item.option
                });
            },

            autocompletechange: "_removeIfInvalid"
        });
    },

    _createShowAllButton: function() {
        var input = this.input,
            wasOpen = false;

        $("<a class='b-combobox__toggle'><i class='ico ico-mini-arrow ico-rotate-270 ui-selectmenu-icon ui-icon'></i></a>")
            .attr("tabIndex", -1)
            .attr("title", "Показать все варианты")
            //.tooltip()
            .appendTo(this.wrapper)
            .on("mousedown", function() {
                wasOpen = input.autocomplete("widget").is(":visible");
            })
            .on("click", function() {
                input.trigger("focus");

                // Close if already visible
                if (wasOpen) {
                    return;
                }

                // Pass empty string as value to search for, displaying all results
                input.autocomplete("search", "");
            });
    },

    _source: function(request, response) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
        response(this.element.children("option").map(function() {
            var text = $(this).text();
            if (this.value && (!request.term || matcher.test(text)))
                return {
                    label: text,
                    value: text,
                    option: this
                };
        }));
    },

    _removeIfInvalid: function(event, ui) {

        // Selected an item, nothing to do
        if (ui.item) {
            return;
        }

        // Search for a match (case-insensitive)
        var value = this.input.val(),
            valueLowerCase = value.toLowerCase(),
            valid = false;
        this.element.children("option").each((option)=> {
            if ($(option).text().toLowerCase() === valueLowerCase) {
                option.selected = valid = true;
                this.element.valid();
                return false;
            }
        });


        // Found a match, nothing to do
        if (valid) {
            return;
        }

        // Remove invalid value
        this.input.val("");
        this.element.val("").valid();
        this.input.autocomplete("instance").term = "";
    },

    _destroy: function() {
        this.wrapper.remove();
        this.element.show();
    }
});
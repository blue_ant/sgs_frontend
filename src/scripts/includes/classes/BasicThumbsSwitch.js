class BasicThumbsSwitch {
    constructor(el, opts) {
        let $el = $(el);
        let $thumbs = $el.find('.b-sldwiththubms__thumbs:first  > .b-sldwiththubms__thumb');
        let defaultOpts = {
        	effect: "fade",
            onlyExternal: true,
        };
        if (opts) $.extend(true, defaultOpts, opts);
        this.swiperInst = new Swiper($el.find('.swiper-container:first'), defaultOpts);


        $thumbs.each((index, el)=> {
        	$(el).on('click', (event)=> {
        		event.preventDefault();
        		this.swiperInst.slideTo(index);
        	});
        });

        this.$el = $el;
        this.$thumbs = $thumbs;
    }

    destroy() {
        this.swiperInst.destroy();
        this.thumbs.off("click");
    }
}
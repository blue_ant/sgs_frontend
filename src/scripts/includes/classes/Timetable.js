class Timetable {
    constructor(selector) {
        let $el = $(selector).eq(0);
        let $daysCont = $el.find(".b-timetbl__days:first");


        this.timetableDaysSliderInstance = new Swiper($daysCont, {
            freeMode: true,
            slidesPerView: 15.66,
        });

        let tableSliders = [];

        $el.find(".b-timetbl__slider").each(function(index, el) {

            let tmpTimetableCalendarSliderInstance = new Swiper(el, {
                freeMode: true,
                slidesPerView: "auto",
            });

            tableSliders.push(tmpTimetableCalendarSliderInstance);
        });


        $daysCont.find('.b-timetbl__daycont').on('click', (event) => {
            event.preventDefault();
            window.requestAnimationFrame(() => {
                let $day = $(event.currentTarget);
                this.$daysCont.find('.b-timetbl__daycont').removeClass('b-timetbl__daycont--active');
                $day.addClass('b-timetbl__daycont--active');
                $.map(tableSliders, function(item) {
                    item.slideTo($day.parent().index());
                });

            });
        });

        this.$el = $el;
        this.$daysCont = $daysCont;
    }

}
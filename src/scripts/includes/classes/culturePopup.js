class CulturePopup {

    init() {
        this.swiper = new Swiper('#cultureModal .swiper-container', {
            prevButton: '.b-culture-popup__swiper_prev',
            nextButton: '.b-culture-popup__swiper_next',
            preloadImages: false,
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            onInit: function(swiper) {
                const title = swiper.container.prev();
                title.html(`Фото 1 из ${swiper.slides.length}`);
            },
            onSlideChangeStart: function(swiper) {
                const title = swiper.container.prev();
                title.html(`Фото ${swiper.realIndex+1} из ${swiper.slides.length}`);
            }
        });
        this.scroll = new PerfectScrollbar('#cultureModal [data-field="description"]');
    }

    showPopupEvt() {
        this.swiper.update(true);
        this.scroll.update();
    }

    generatePropsContent(prop) {
        return prop.reduce((accum, element) => {
            return accum + `<div class="b-culture-popup__table_row">
                                <div class="b-culture-popup__table_cell b-culture-popup__table_cell--grey">${element.title}: </div>
                                <div class="b-culture-popup__table_cell">${element.value}</div>
                            </div>`;
        }, '');
    }

    generateGalleryContent(prop) {
        this.swiper.removeAllSlides();
        this.swiper.prependSlide(prop.map((element) => {
            return `<div class="swiper-slide b-culture-popup__swiper-slide"><div data-background='${element}' class="swiper-lazy"><div class="swiper-lazy-preloader"></div></div>`;
        }));
        this.swiper.slideTo(0);
        this.swiper.onResize();
        return 'show';
    }

    generateContent(rootPopup, jsonRequest) {
        /* clear new iteration */
        rootPopup.find('.no-delimiter').removeClass('no-delimiter');

        /* update popup content for json tag */
        rootPopup.find('.js-culture-content').each((key, el) => {
            const element = $(el);
            const jsonField = element.data('field');

            let html = '';

            if (jsonField == 'description') {
                el = el.parentElement;
            }

            /* custom logic for different json fields */
            switch (jsonField) {
                case 'title':
                case 'description':
                    if (jsonRequest[jsonField] != undefined) {
                        html = jsonRequest[jsonField];
                    }

                    break;
                case 'props':
                    if (jsonRequest[jsonField] != undefined && jsonRequest[jsonField].length > 0) {
                        html = this.generatePropsContent(jsonRequest[jsonField]);

                    }
                    break;
                case 'gallery':
                    if (jsonRequest[jsonField] != undefined && jsonRequest[jsonField].length != 0) {
                        html = this.generateGalleryContent(jsonRequest[jsonField]);
                    }
                    if (html !== '') {
                        element.show();
                    } else {
                        element.hide();
                    }
                    break;
            }

            if (jsonField !== 'gallery') {

                element.html(html);
            }
        });
    }
    constructor(actionsItemsSelector) {
        if ($('#cultureModal').length == 1) {
            this.init();
            const self = this;
            $('.b-timetbl__grid').on('click', actionsItemsSelector, function() {
                window.pagePreloader.show();
                $.ajax({
                    url: $(this).data('href'),
                    dataType: 'json'
                }).done((jsonRequest) => {
                    window.pagePreloader.hide();
                    $.fancybox.open({
                        src: '#cultureModal',
                        opts: {
                            beforeShow: function(instance, current /*, e*/ ) {
                                self.generateContent($(current.src), jsonRequest);
                            },
                            afterShow: function() {
                                self.showPopupEvt();
                            },
                        }
                    });
                }).fail(() => {
                    window.pagePreloader.hide();
                });
            });
        }
    }
}
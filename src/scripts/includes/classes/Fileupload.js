//TODO: finalize file uploading logic
class Fileupload {
    constructor(el, opts) {
        let self = this;
        let $el = $(el);
        let $fileInp = $el.find('input[type=file]:first');

        $fileInp.fileupload({
            dataType: 'json',
            add: (e, data) => {
                _.forEach(data.originalFiles, function(file) {
                    var $progressbar = $('<div class="b-fileupload__progress"><div class="b-fileupload__progress-text">' + file.name + '</div><span class="b-fileupload__percents">0%</span><a href="#" class="b-fileupload__delete">&times;</a></div>');
                    $progressbar.progressbar({
                        value: 50,
                        classes: {
                            "ui-progressbar-value": "b-fileupload__progressbar",
                            "ui-progressbar-complete": "b-fileupload__progressbar--complete",
                        }
                    });
                    //self.percentsTextEl = $progressbar.find('.b-fileupload__percents:first').get(0);
                    $el.before($progressbar);
                });
            },
            /*progressall: (e, data) => {
                
            },
            done: (e, data) => {
                //...
            },*/
        });

        self.$el = $el;
        self.$fileInp = $fileInp;
    }

    destroy() {
        this.$el.progressbar("destroy");
    }
}
$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно"
);
class InteractiveForm {
    constructor(opts) {

        this.$form = $(opts.el).eq(0);

        let $form = this.$form;
        if ($form.data("action")) {
            $form.prop('action', $form.data("action"));
        }

        $form.find('[type="tel"]').inputmask({
            mask: "+7 (999) 999-99-99",
            showMaskOnHover: false,
        });

        $form.find("select").not(".b-combobox").selectmenu({
            classes: {
                "ui-selectmenu-button": "form-control",
                "ui-selectmenu-icon": "ico ico-mini-arrow ico-rotate-270"
            },
            icons: { button: "" },
            position: {
                at: 'left bottom+2px'
            }
        }).on("selectmenuchange", function(event, ui) {
            $(event.target).valid();
        });

        $form.find(".b-combobox").combobox();

        $form.find(".b-bigradio__inp").checkboxradio({
            classes: {
                "ui-checkboxradio": "",
                "ui-checkboxradio-label": "",
                "ui-checkboxradio-checked": "b-bigradio__btn--active"
                //"ui-checkboxradio-icon": ""
            },
            icon: false,
        });

        let validatorOpts = {
            rules: {
                phone: {
                    required: true,
                    regex: /\+7\s\(\d\d\d\)\s\d\d\d\-\d\d\-\d\d/,
                },
            },
            onfocusout: (el, event) => {
                $(el).valid();
            },
            onclick: false,
            focusCleanup: false,
            ignore: [],
            submitHandler: opts.submitHandler || this.standartFormHandler, //(form)=>{}
            errorPlacement: ($errorLabel, $el) => {
                /*if ($el.is(".b-custom-checkbox__input")) {
                    return true;
                } else {
                    $errorLabel.insertAfter($el);
                }*/

                return false;
            },

        };

        if (opts.validatorParams) {
            $.extend(true, validatorOpts, opts.validatorParams);
        }

        this.validator = $form.validate(validatorOpts);
    }

    standartFormHandler( /*form*/ ) {
        alert("Отправка формы...");
    }

    destroy() {
        this.validator.destroy();
        this.$form.find('input').inputmask('remove');
        this.$form.find("select").not(".b-combobox").selectmenu("destroy");
        this.$form.find(".b-combobox").combobox("destroy");
        this.$form.find(".b-bigradio__inp").checkboxradio("destroy");
    }
}
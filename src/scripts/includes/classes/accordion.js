class Accordion {
    constructor(el, opts) {
        let $el = $(el);
        let defaultOpts = {
            active: false,
            collapsible: true,
            heightStyle: "content",
            animate: 200,
            classes: {
                "ui-accordion-header": "",
                "ui-accordion-header-collapsed": "",
                "ui-accordion-content": "",
            },
            icons: {
                "header": "b-big-accordion__icon ico ico-arrow-round",
                "activeHeader": "b-big-accordion__icon b-big-accordion__icon--active ico ico-arrow-round"
            }
        };

        if (opts) $.extend(true, defaultOpts, opts);
        $el.accordion(defaultOpts);
        this.$el = $el;
    }

    destroy() {
        this.$el.accordion("destroy");
    }
}
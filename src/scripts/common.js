/*=require ./includes/chunks/google_map_styles.js */
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/chunks/validator_default_messages_override.js */
/*=require ./includes/classes/*.js */

/*function imgToInlineSvg(selector) {
    $(selector).each(function() {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            // Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');
    });
}*/

$(document).ready(function() {

    //imgToInlineSvg(".b-header__pic");

    $(".b-modalform > form").each(function(index, el) {
        new InteractiveForm({el:el});
    });

    document.querySelectorAll(".b-notifications__close-btn, .b-notify-counter").forEach((item) => {

        item.onclick = (event) => {
            event.preventDefault();
            event.stopPropagation();
            document.querySelector(".b-notifications").classList.toggle("b-notifications--active");
            return false;
        };
    });

    window.pagePreloader = new Preloader("#pagePreloader");
    window.pagePreloader.hide();

    // tmp
    document.querySelectorAll(".b-header__link").forEach((item) => {
        item.onclick = (event) => {
            event.preventDefault();
            event.currentTarget.classList.toggle("b-header__link--active");
            return false;
        };
    });
    new CulturePopup('.js-culture-popup');
    $('.b-nav__leaf').filter(function() {
        return this.href == window.location.href;
    }).addClass('b-nav__leaf--active');



});